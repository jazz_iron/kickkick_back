-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: match
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'match'
--

--
-- Dumping routines for database 'match'
--
/*!50003 DROP PROCEDURE IF EXISTS `setMatchHistory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `setMatchHistory`()
BEGIN

	DECLARE i INT DEFAULT 1;
    
    START TRANSACTION;
    ## 백업테이블 삭제
	DELETE FROM `match`.back_matchhistory;
    
    ## 백업테이블에 기존데이터 넣기
    INSERT INTO `match`.back_matchhistory SELECT * FROM matchhistory;

    ## 날짜 업데이트
    update `match`.temp_matchhistory
	set matchDate = concat('2019',substr(matchDate,5,6))
	where matchDate between 20180101 and 20180530;
    COMMIT;
    
    ## 라운드생성
	WHILE i <= 38 DO

       update `match`.temp_matchhistory
		set matchRound = i
		where matchId in(
		select matchId
        from(
			select matchId from 
			`match`.temp_matchhistory
            where matchRound is null
			order by matchDate asc
            limit 0,10
			)b
		);
	
		SET i = i + 1;
    END WHILE;
    
    
    #경기테이블 삭제
    DELETE FROM `match`.matchhistory;
    
    #경기테이블 입력
    INSERT INTO `match`.matchhistory SELECT * FROM temp_matchhistory;
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `teamAvgStat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `teamAvgStat`()
BEGIN

	DECLARE i INT DEFAULT 1;
    DECLARE j INT DEFAULT 1;
    
	DECLARE teamId INT;
	DECLARE areaType VARCHAR(45);  
    DECLARE pointGame DOUBLE;
	DECLARE wins INT;
    DECLARE defeats INT;
    DECLARE scoredGame DOUBLE;
    DECLARE concededGame DOUBLE;
    DECLARE draw INT;
    DECLARE totalGoal DOUBLE;
    DECLARE over25 INT;
    DECLARE over35 INT;
	DECLARE bothGoal INT;

	DELETE FROM `match`.teamavg;
     
	-- team average
    -- home 
	WHILE i <= 20 DO
     
	set teamId = i;
    set areaType = 'home';
    
	set pointGame =(
		SELECT
		round(sum(homeGrade)/(SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId  = i
		and (homeGrade = '3' or homeGrade = '1')
	);
	
    set wins =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)),0) as num
		from `match`.matchhistory
		where homeTeamId  = i and homeGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)),0) as num
		from `match`.matchhistory
		where homeTeamId  = i and homeGrade = '0'
	);
    set scoredGame =(
		select
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId=i
	);
    set concededGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId=i
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i) ,0) as num
		from `match`.matchhistory
		where homeTeamId  = i
		and homeGrade = '1'
	);
    set totalGoal =(
		select  
		round(sum(h.num+a.num),2) from (
		select
		sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i) as num
		from `match`.matchhistory
		where homeTeamId=i)h,
		(select 
		sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)as num
		from `match`.matchhistory
		where homeTeamId=i)a
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where homeTeamId  = i
		group by matchId
		having sum(homeScore+awayScore) > 2
		)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where homeTeamId  = i
		group by matchId
		having sum(homeScore+awayScore) > 3
		)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from `match`.matchhistory
		where homeTeamId  = i
		and (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
     
     
	 SET i = i + 1;
	 
	 END WHILE;  
     
	WHILE j <= 20 DO
     
	set teamId = j;
    set areaType = 'away';
    
	set pointGame =(
		SELECT 
		round(sum(awayGrade)/(SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),2) as num
		from `match`.matchhistory
		where awayTeamId  = j
		and (awayGrade = '3' or awayGrade = '1')
	);
	
    set wins =(
		SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j)),0) as num
		from `match`.matchhistory
		where awayTeamId  = j and awayGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j)),0) as num
		from `match`.matchhistory
		where awayTeamId  = j and awayGrade = '0'
	);
    set scoredGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),2) as num
		from `match`.matchhistory
		where awayTeamId=j
	);
    set concededGame =(
		select 
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),2) as num
		from `match`.matchhistory
		where awayTeamId=j
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j) ,0) as num
		from `match`.matchhistory
		where awayTeamId =j
		and awayGrade = '1'
	);
    set totalGoal =(
		select  
		round(sum(h.num+a.num),2) from (
		select
		sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j) as num
		from `match`.matchhistory
		where awayTeamId=j)h,
		(select 
		sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j) as num
		from `match`.matchhistory
		where awayTeamId=j)a
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where awayTeamId  = j
		group by matchId
		having sum(homeScore+awayScore) > 2
		)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where awayTeamId  = j
		group by matchId
		having sum(homeScore+awayScore) > 3
		)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),0) as num  
		from `match`.matchhistory
		where awayTeamId  = j
		and (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
     
     SET j = j + 1;
	 
     END WHILE;      
	
     
    -- league average 
    
	set teamId = 00;
    -- home
    set areaType = 'home';
    
	set pointGame =(
		SELECT
		round(sum(homeGrade)/(SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
		where (homeGrade = '3' or homeGrade = '1')
        );
	
    set wins =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where 1=1)),0) as num
		from `match`.matchhistory
		where  homeGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory)),0) as num
		from `match`.matchhistory
		where homeGrade = '0'
	);
    
    set scoredGame =(
		select
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set concededGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory) ,0) as num
		from `match`.matchhistory
		where  homeGrade = '1' or awayGrade ='1'
	);
    set totalGoal =(
		select 
		round(sum(homeScore + awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 2)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 3)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from `match`.matchhistory
		where(homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
    
    
    -- away
	set areaType = 'away';
     
	set pointGame =(
	SELECT 
		round(sum(awayGrade)/(SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
		where (awayGrade = '3' or awayGrade = '1')
        );
	
    set wins =(
	SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where 1=1)),0) as num
		from `match`.matchhistory
		where awayGrade = '3'
	);
	set defeats =(
		SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory )),0) as num
		from `match`.matchhistory
		where awayGrade = '0'
	);
    set scoredGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set concededGame =(
		select 
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory) ,0) as num
		from `match`.matchhistory
		where  homeGrade = '1' or awayGrade ='1'
	);
    set totalGoal =(
		select 
		round(sum(homeScore + awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set over25 =(
		select
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 2)t
	);
	set over35 =(
		select
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 3)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from `match`.matchhistory
		where (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `teamRank` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `teamRank`()
BEGIN

	DECLARE i INT DEFAULT 1;
    DECLARE matchCnt VARCHAR(50);
    DECLARE rank INT;
	DECLARE teamId INT;
	DECLARE teamName VARCHAR(200);  
    DECLARE grade INT;
	DECLARE win INT;
    DECLARE draw INT;
    DECLARE lose INT;
    DECLARE goalPoint INT;
    DECLARE losePoint INT;
    DECLARE gap INT;
	DECLARE j INT DEFAULT 1;
	
	DELETE FROM `match`.teamrank;

	WHILE i <= 20 DO
    
		SET teamId =  i;
		
		SET matchCnt = (selecT count(*) from `match`.matchhistory where homeTeamId = i or awayTeamId = i);
			
		SET grade =
		(select sum(h.grade + a.grade) from 
        (SELECT sum(homeGrade) as grade 
        FROM `match`.matchhistory
		where homeTeamId = i)h,
		(SELECT sum(awayGrade) as grade 
        FROM `match`.matchhistory
		where awayTeamId = i)a);
		
				
		SET win = ( select sum(a.cnt+b.cnt) as winCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and homeGrade = '3')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and awayGrade = '3')b);
					
		SET draw = ( select sum(a.cnt+b.cnt) as DrawCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and homeGrade = '1')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and awayGrade = '1')b);
					
		SET lose =  (select sum(a.cnt+b.cnt) as loseCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and awayGrade = '3')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and homeGrade = '3')b);
		
		SET goalPoint = (select sum(a.score + b.score) as sumScore from (SELECT sum(homeScore) as score FROM `match`.matchhistory where homeTeamId = i) a , (select sum(awayScore) as score FROM `match`.matchhistory where awayTeamId = i)b);
		SET losePoint = (select sum(a.score + b.score) as sumScore from (SELECT sum(awayScore) as score FROM `match`.matchhistory where homeTeamId = i) a , (select sum(homeScore) as score FROM `match`.matchhistory where awayTeamId = i)b);
		
		SET gap = (goalPoint - losePoint);

		
		INSERT INTO `match`.teamrank (matchCnt, teamId, grade, win, draw, lose, goalPoint, losePoint, gap) VALUES (matchCnt, teamId, grade, win, draw, lose, goalPoint, losePoint, gap);


		SET i = i + 1;

	END WHILE;
    
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `teamRankAdd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `teamRankAdd`()
BEGIN

    DECLARE rank INT;
	DECLARE teamId INT;
	DECLARE teamName VARCHAR(200);  
	DECLARE j INT DEFAULT 1;
	
    WHILE j <= 20 DO
    
		SET rank = (select k.rank 
			from ( SELECT r.teamId ,( SELECT COUNT(*) + 1 FROM `match`.teamrank where (grade > r.grade or (grade = r.grade and (gap > r.gap))) ) AS rank FROM `match`.teamrank r ORDER BY grade ASC, gap DESC)k  where k.teamId = j);
					
		SET teamName = (select teamshortNm
		 from `match`.teaminfo t , `match`.teamrank r
		  where t.teamId = r.teamId
		  and t.teamId = j );

		UPDATE `match`.teamrank r
		SET  r.rank = rank
		where r.teamId = j;
		
		UPDATE `match`.teamrank t
		SET  t.teamName = teamName
		where t.teamId = j;
						
		SET j = j + 1;
    
    END WHILE;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-30  1:32:24
