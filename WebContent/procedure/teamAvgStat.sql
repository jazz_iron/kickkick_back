
DELIMITER $$

DROP PROCEDURE IF EXISTS teamAvgStat$$
CREATE PROCEDURE teamAvgStat()

BEGIN

	DECLARE i INT DEFAULT 1;
    DECLARE j INT DEFAULT 1;
    
	DECLARE teamId INT;
	DECLARE areaType VARCHAR(45);  
    DECLARE pointGame DOUBLE;
	DECLARE wins INT;
    DECLARE defeats INT;
    DECLARE scoredGame DOUBLE;
    DECLARE concededGame DOUBLE;
    DECLARE draw INT;
    DECLARE totalGoal DOUBLE;
    DECLARE over25 INT;
    DECLARE over35 INT;
	DECLARE bothGoal INT;

	DELETE FROM `match`.teamavg;
     
	-- team average
    -- home 
	WHILE i <= 20 DO
     
	set teamId = i;
    set areaType = 'home';
    
	set pointGame =(
		SELECT
		round(sum(homeGrade)/(SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId  = i
		and (homeGrade = '3' or homeGrade = '1')
	);
	
    set wins =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)),0) as num
		from `match`.matchhistory
		where homeTeamId  = i and homeGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)),0) as num
		from `match`.matchhistory
		where homeTeamId  = i and homeGrade = '0'
	);
    set scoredGame =(
		select
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId=i
	);
    set concededGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),2) as num
		from `match`.matchhistory
		where homeTeamId=i
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i) ,0) as num
		from `match`.matchhistory
		where homeTeamId  = i
		and homeGrade = '1'
	);
    set totalGoal =(
		select  
		round(sum(h.num+a.num),2) from (
		select
		sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i) as num
		from `match`.matchhistory
		where homeTeamId=i)h,
		(select 
		sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i)as num
		from `match`.matchhistory
		where homeTeamId=i)a
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where homeTeamId  = i
		group by matchId
		having sum(homeScore+awayScore) > 2
		)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where homeTeamId  = i
		group by matchId
		having sum(homeScore+awayScore) > 3
		)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory where homeTeamId = i),0) as num  
		from `match`.matchhistory
		where homeTeamId  = i
		and (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
     
     
	 SET i = i + 1;
	 
	 END WHILE;  
     
	WHILE j <= 20 DO
     
	set teamId = j;
    set areaType = 'away';
    
	set pointGame =(
		SELECT 
		round(sum(awayGrade)/(SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),2) as num
		from `match`.matchhistory
		where awayTeamId  = j
		and (awayGrade = '3' or awayGrade = '1')
	);
	
    set wins =(
		SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j)),0) as num
		from `match`.matchhistory
		where awayTeamId  = j and awayGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j)),0) as num
		from `match`.matchhistory
		where awayTeamId  = j and awayGrade = '0'
	);
    set scoredGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),2) as num
		from `match`.matchhistory
		where awayTeamId=j
	);
    set concededGame =(
		select 
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),2) as num
		from `match`.matchhistory
		where awayTeamId=j
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j) ,0) as num
		from `match`.matchhistory
		where awayTeamId =j
		and awayGrade = '1'
	);
    set totalGoal =(
		select  
		round(sum(h.num+a.num),2) from (
		select
		sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j) as num
		from `match`.matchhistory
		where awayTeamId=j)h,
		(select 
		sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j) as num
		from `match`.matchhistory
		where awayTeamId=j)a
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where awayTeamId  = j
		group by matchId
		having sum(homeScore+awayScore) > 2
		)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId = j),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		where awayTeamId  = j
		group by matchId
		having sum(homeScore+awayScore) > 3
		)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory where awayTeamId =j),0) as num  
		from `match`.matchhistory
		where awayTeamId  = j
		and (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
     
     SET j = j + 1;
	 
     END WHILE;      
	
     
    -- league average 
    
	set teamId = 00;
    -- home
    set areaType = 'home';
    
	set pointGame =(
		SELECT
		round(sum(homeGrade)/(SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
		where (homeGrade = '3' or homeGrade = '1')
        );
	
    set wins =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where 1=1)),0) as num
		from `match`.matchhistory
		where  homeGrade = '3'
	);
	set defeats =(
		SELECT
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory)),0) as num
		from `match`.matchhistory
		where homeGrade = '0'
	);
    
    set scoredGame =(
		select
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set concededGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory) ,0) as num
		from `match`.matchhistory
		where  homeGrade = '1' or awayGrade ='1'
	);
    set totalGoal =(
		select 
		round(sum(homeScore + awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set over25 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 2)t
	);
	set over35 =(
		select 
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 3)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from `match`.matchhistory
		where(homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);
    
    
    -- away
	set areaType = 'away';
     
	set pointGame =(
	SELECT 
		round(sum(awayGrade)/(SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
		where (awayGrade = '3' or awayGrade = '1')
        );
	
    set wins =(
	SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory where 1=1)),0) as num
		from `match`.matchhistory
		where awayGrade = '3'
	);
	set defeats =(
		SELECT 
		round((count(*) *100 / (SELECT count(*) FROM `match`.matchhistory )),0) as num
		from `match`.matchhistory
		where awayGrade = '0'
	);
    set scoredGame =(
		select 
		round(sum(awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set concededGame =(
		select 
		round(sum(homeScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set draw =(
		select 
		round(count(*) *100 / (SELECT count(*) FROM `match`.matchhistory) ,0) as num
		from `match`.matchhistory
		where  homeGrade = '1' or awayGrade ='1'
	);
    set totalGoal =(
		select 
		round(sum(homeScore + awayScore) / (SELECT count(*) FROM `match`.matchhistory),2) as num
		from `match`.matchhistory
	);
    set over25 =(
		select
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 2)t
	);
	set over35 =(
		select
		round(sum(t.cnt) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from(
		select  matchId, count(*) as cnt
		from `match`.matchhistory
		group by matchId
		having sum(homeScore+awayScore) > 3)t
	);
	set bothGoal =(
		select 
		round(count(*) * 100 /  (SELECT count(*) FROM `match`.matchhistory),0) as num  
		from `match`.matchhistory
		where (homeScore != 0 and awayScore != 0)
	);
    
	INSERT INTO `match`.teamavg (teamId,type,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal) 
	VALUES (teamId,areaType,pointGame,wins,defeats,scoredGame,concededGame,draw,totalGoal,over25,over35,bothGoal);

END$$

DELIMITER $$