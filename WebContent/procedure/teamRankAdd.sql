DELIMITER $$

DROP PROCEDURE IF EXISTS teamRankAdd$$
CREATE PROCEDURE teamRankAdd()

BEGIN

    DECLARE rank INT;
	DECLARE teamId INT;
	DECLARE teamName VARCHAR(200);  
	DECLARE j INT DEFAULT 1;
	
    WHILE j <= 20 DO
    
		SET rank = (select k.rank 
			from ( SELECT r.teamId ,( SELECT COUNT(*) + 1 FROM `match`.teamrank where (grade > r.grade or (grade = r.grade and (gap > r.gap))) ) AS rank FROM `match`.teamrank r ORDER BY grade ASC, gap DESC)k  where k.teamId = j);
					
		SET teamName = (select teamshortNm
		 from `match`.teaminfo t , `match`.teamrank r
		  where t.teamId = r.teamId
		  and t.teamId = j );

		UPDATE `match`.teamrank r
		SET  r.rank = rank
		where r.teamId = j;
		
		UPDATE `match`.teamrank t
		SET  t.teamName = teamName
		where t.teamId = j;
						
		SET j = j + 1;
    
    END WHILE;
    

END$$

DELIMITER $$
