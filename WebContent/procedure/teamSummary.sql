DELIMITER $$

DROP PROCEDURE IF EXISTS teamSummary$$
CREATE PROCEDURE teamSummary()

BEGIN

	DECLARE i INT DEFAULT 1;
    DECLARE teamId INT;
    DECLARE pointH INT; 
    DECLARE pointA INT;
    DECLARE scoredH INT;
    DECLARE scoredA INT;
    DECLARE concededH INT;
    DECLARE concededA INT;
    DECLARE ppgh DOUBLE;
    DECLARE ppga DOUBLE;

	
	DELETE FROM `match`.teamsummary;

	WHILE i <= 20 DO
    
		SET teamId =  i;
		
        SET pointH = (	select round(sum(homeGrade) * 100 /sumCnt.sumCnt,0) as num
						FROM 
						`match`.matchhistory , 
							(
								select sum(a.num+ b.num) as sumCnt from
								(select sum(homeGrade) as num
								from `match`.matchhistory
								where homeTeamId=i
								and state ='after')a,
								(select sum(awayGrade) as num
								from `match`.matchhistory
								where AwayTeamId=i
								and state ='after')b
							) sumCnt
						where homeTeamId= i);
                        
		SET pointA = (	SELECT 100 - round(sum(homeGrade) * 100 /sumCnt.sumCnt,0) as num
						FROM 
						`match`.matchhistory , 
							(
								select sum(a.num+ b.num) as sumCnt from
								(select sum(homeGrade) as num
								from `match`.matchhistory
								where homeTeamId=i
								and state ='after')a,
								(select sum(awayGrade) as num
								from `match`.matchhistory
								where AwayTeamId=i
								and state ='after')b
							) sumCnt
						where homeTeamId= i);               
		
        
        SET scoredH = (SELECT round(sum(homeScore) * 100 /sumCnt.sumCnt,0) as num
						FROM 
						`match`.matchhistory , 
							(
							select sum(a.num+b.num) as sumCnt from (
								(select sum(homeScore) as num
								from `match`.matchhistory
								where homeTeamId=i
								and state ='after')a,
								(select sum(awayScore) as num
								from `match`.matchhistory
								where AwayTeamId=i
								and state ='after')b
							))	 sumCnt
						where homeTeamId=i);
                        
		 SET scoredA = (SELECT 100 - round(sum(homeScore) * 100 /sumCnt.sumCnt,0) as num
						FROM 
						`match`.matchhistory , 
							(
							select sum(a.num+b.num) as sumCnt from (
								(select sum(homeScore) as num
								from `match`.matchhistory
								where homeTeamId=i
								and state ='after')a,
								(select sum(awayScore) as num
								from `match`.matchhistory
								where AwayTeamId=i
								and state ='after')b
							))	 sumCnt
						where homeTeamId=i);      
                        
		SET concededH = (SELECT round(sum(awayScore) * 100 /sumCnt.sumCnt,0)
							FROM `match`.matchhistory, 
								(select sum(a.num+b.num) as sumCnt from (
									(select sum(awayScore) as num
									from `match`.matchhistory
									where homeTeamId=i
									and state = 'after') a,
									(select sum(homeScore) as num
									from `match`.matchhistory
									where AwayTeamId=i
									and state = 'after') b
									)) sumCnt
							where homeTeamId=i);
                            
		SET concededA = (SELECT 100 - round(sum(awayScore) * 100 /sumCnt.sumCnt,0)
					FROM `match`.matchhistory, 
						(select sum(a.num+b.num) as sumCnt from (
							(select sum(awayScore) as num
							from `match`.matchhistory
							where homeTeamId=i
							and state = 'after') a,
							(select sum(homeScore) as num
							from `match`.matchhistory
							where AwayTeamId=i
							and state = 'after') b
							)) sumCnt
					where homeTeamId=i);     
                    
		SET ppgh = (SELECT round(sum(homeGrade) / (SELECT count(*) FROM `match`.matchhistory where homeTeamId ='2' and state ='after'),1) 
					FROM `match`.matchhistory
					WHERE homeTeamId =i
					AND state ='after');
		SET ppga = (SELECT round(sum(awayGrade) / (SELECT count(*) FROM `match`.matchhistory where awayTeamId ='2' and state ='after'),1) 
					FROM `match`.matchhistory
					WHERE awayTeamId =i		
					AND state ='after');
		
		INSERT INTO `match`.teamsummary (teamId, pointH, scoredH, concededH, pointA, scoredA, concededA, ppgh, ppga) VALUES (teamId, pointH, scoredH, concededH, pointA, scoredA, concededA, ppgh, ppga);

		SET i = i + 1;

	END WHILE;
    
    

END$$

DELIMITER $$