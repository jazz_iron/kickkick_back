DELIMITER $$

DROP PROCEDURE IF EXISTS setMatchHistoryScore$$
CREATE PROCEDURE setMatchHistoryScore()

BEGIN

	declare done INTEGER DEFAULT 0;
	DECLARE v_homeScore VARCHAR(10); 
	DECLARE v_awayScore VARCHAR(10); 
	DECLARE v_htHome VARCHAR(20); 
	DECLARE v_htAway  VARCHAR(20); 
	DECLARE v_homeGrade VARCHAR(20); 
	DECLARE v_awayGrade VARCHAR(20); 
	DECLARE v_homeTeamId VARCHAR(45); 
	DECLARE v_awayTeamId VARCHAR(45); 

    
    declare openCursor cursor for SELECT homeScore, awayScore, htHome, htAway, homeGrade, awayGrade, homeTeamId, awayTeamId FROM `match`.matchhistory where state = 'before' order by matchRound asc;
    
    declare continue handler for NOT FOUND SET done = true;
    
    
    OPEN openCursor;
        
			read_loop: LOOP
				
				fetch openCursor INTO v_homeScore, v_awayScore, v_htHome, v_htAway, v_homeGrade, v_awayGrade, v_homeTeamId, v_awayTeamId;
                
                
                IF done THEN LEAVE read_loop;
				END IF;
                
                
				SELECT @homeScore := homeScore , @awayScore := awayScore, @htHome := htHome, @htAway := htAway, @homeGrade := homeGrade, @awayGrade := awayGrade , v_homeTeamId, v_awayTeamId, length(v_homeTeamId)
				FROM `match`.temp_matchhistory
				WHERE length(matchDate) < 1
				and homeTeamId = v_homeTeamId
				and awayTeamId = v_awayTeamId;
                
                IF @homeScore is not null THEN
                
					update `match`.matchhistory set homeScore = @homeScore, awayScore = @awayScore, htHome = @htHome, htAway = @htAway, homeGrade = @homeGrade, awayGrade = @awayGrade, state ='after'
					where homeTeamId = v_homeTeamId and awayTeamId = v_awayTeamId;
				END IF;
                
				SELECT @homeScore := null, @awayScore := null , @awayScore := null, @htHome := null, @htAway := null, @homeGrade := null, @awayGrade := null;
				
            END LOOP read_loop;
                
                
    close openCursor;
    

END$$

DELIMITER $$
