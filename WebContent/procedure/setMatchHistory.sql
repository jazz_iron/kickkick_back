DELIMITER $$

DROP PROCEDURE IF EXISTS setMatchHistory$$
CREATE PROCEDURE setMatchHistory()

BEGIN

	DECLARE i INT DEFAULT 1;
    
    START TRANSACTION;
    ## 백업테이블 삭제
	DELETE FROM `match`.back_matchhistory;
    
    ## 백업테이블에 기존데이터 넣기
    INSERT INTO `match`.back_matchhistory SELECT * FROM matchhistory;

    ## 날짜 업데이트
    update `match`.temp_matchhistory
	set matchDate = concat('2019',substr(matchDate,5,6))
	where matchDate between 20180101 and 20180530;
    COMMIT;
    
    ## 라운드생성
	WHILE i <= 38 DO

       update `match`.temp_matchhistory
		set matchRound = i
		where matchId in(
		select matchId
        from(
			select matchId from 
			`match`.temp_matchhistory
            where matchRound is null
			order by matchDate asc
            limit 0,10
			)b
		);
	
		SET i = i + 1;
    END WHILE;
    
    
    #경기테이블 삭제
    DELETE FROM `match`.matchhistory;
    
    #경기테이블 입력
    INSERT INTO `match`.matchhistory SELECT * FROM temp_matchhistory;

END$$

DELIMITER $$