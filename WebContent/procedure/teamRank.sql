DELIMITER $$

DROP PROCEDURE IF EXISTS teamRank$$
CREATE PROCEDURE teamRank()

BEGIN

	DECLARE i INT DEFAULT 1;
    DECLARE matchCnt VARCHAR(50);
    DECLARE rank INT;
	DECLARE teamId INT;
	DECLARE teamName VARCHAR(200);  
    DECLARE grade INT;
	DECLARE win INT;
    DECLARE draw INT;
    DECLARE lose INT;
    DECLARE goalPoint INT;
    DECLARE losePoint INT;
    DECLARE gap INT;
	DECLARE j INT DEFAULT 1;
	
	DELETE FROM `match`.teamrank;

	WHILE i <= 20 DO
    
		SET teamId =  i;
		
		SET matchCnt = (selecT count(*) from `match`.matchhistory where homeTeamId = i or awayTeamId = i);
			
		SET grade =
		(select sum(h.grade + a.grade) from 
        (SELECT sum(homeGrade) as grade 
        FROM `match`.matchhistory
		where homeTeamId = i)h,
		(SELECT sum(awayGrade) as grade 
        FROM `match`.matchhistory
		where awayTeamId = i)a);
		
				
		SET win = ( select sum(a.cnt+b.cnt) as winCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and homeGrade = '3')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and awayGrade = '3')b);
					
		SET draw = ( select sum(a.cnt+b.cnt) as DrawCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and homeGrade = '1')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and awayGrade = '1')b);
					
		SET lose =  (select sum(a.cnt+b.cnt) as loseCnt from (
					SELECT count(*) as cnt FROM `match`.matchhistory
					where homeTeamId = i
					and awayGrade = '3')a,
					(SELECT count(*) as cnt FROM `match`.matchhistory
					where awayTeamId = i
					and homeGrade = '3')b);
		
		SET goalPoint = (select sum(a.score + b.score) as sumScore from (SELECT sum(homeScore) as score FROM `match`.matchhistory where homeTeamId = i) a , (select sum(awayScore) as score FROM `match`.matchhistory where awayTeamId = i)b);
		SET losePoint = (select sum(a.score + b.score) as sumScore from (SELECT sum(awayScore) as score FROM `match`.matchhistory where homeTeamId = i) a , (select sum(homeScore) as score FROM `match`.matchhistory where awayTeamId = i)b);
		
		SET gap = (goalPoint - losePoint);

		
		INSERT INTO `match`.teamrank (matchCnt, teamId, grade, win, draw, lose, goalPoint, losePoint, gap) VALUES (matchCnt, teamId, grade, win, draw, lose, goalPoint, losePoint, gap);


		SET i = i + 1;

	END WHILE;
    
    

END$$

DELIMITER $$