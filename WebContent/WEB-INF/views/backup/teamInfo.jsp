<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link href="/resources/css/micro.css" rel="stylesheet"/>
	<link href="/resources/css/table.css" rel="stylesheet"/>
	<link href="/resources/css/swiper.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
</head>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>
	<script src="/resources/js/swiper.min.js"></script>
	
	<div class="container fullBackgoundSet-g">
		
		<!-- NAV START -->
		<div class="row teamheaderDiv" id="teamheaderDiv">
			<header class="teamHeader" id ="teamHeader">
				<div id ="teamInfoHtml"></div>
				<div class= "teamHeaderNav"  id="nav">
					<div class="navItem"><div class="navItemTitle" id ="sec00" onclick = "moveSection(this);">요약<div class="teamHeaderNavLine" id="mainNav1" style="display: none;"></div></div></div>
					<div class="navItem"><div class="navItemTitle" id ="sec01" onclick = "moveSection(this);">골통계<div class="teamHeaderNavLine" id="mainNav2" style="display: none;"></div></div></div>
					<div class="navItem"><div class="navItemTitle" id="sec02"onclick = "moveSection(this);">구단통계<div class="teamHeaderNavLine" id="mainNav3" style="display: none;"></div></div></div>
					<div class="navItem"><div class="navItemTitle" id="sec03" onclick = "moveSection(this);">vs리그<div class="teamHeaderNavLine" id="mainNav4" style="display: none;"></div></div></div>
					<div class="navItem"><div class="navItemTitle" id="sec04" onclick = "moveSection(this);">경기<div class="teamHeaderNavLine" id="mainNav5" style="display: none;"></div></div></div>
				</div>
			</header>
		</div>
		<!-- NAV END -->
		
		<!-- 요약 START -->
		<div class="row temaInfoContentDiv swiper-container"  id = "temaInfoContentDiv">
			<div class="swiper-wrapper">
			<div class="row secContentDiv swiper-slide" id ="sec00_content">
				<!-- 팀 랭크 START -->
				<div class="teamRank row">
					<div class= "teamRankInfo"  id="teamRankInfo">
						<div class="teamSubInfoDiv">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="content_item">
										<div class="title">순위</div>
										<div class="content"><span id ="rank"></span></div>
									</div>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="content_item">
										<div class="title">득점</div>
										<div class="content"><span id ="goalRank"></span></div>
									</div>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="content_item">
										<div class="title">득점차</div>
										<div class="content"><span id ="gapRank"></span></div>
									</div>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="content_item">
										<div class="title">승률</div>
										<div class="content"><span id ="winPer"></span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- 팀 랭크 END -->
			
				<div class="homeAdvantageDiv row">
				
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-l">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">홈에서<br>얻는 평균승점</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<div class="homeAdvRank"><span id= "ppghRank"></span>위</div>
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "ppgh"></span></div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-r">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">어웨이에서<br>얻는 평균승점</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<div class="homeAdvRank"><span id= "ppgaRank"></span>위</div>
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "ppga"></span></div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-l">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">홈에서<br>승점획득비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "pointRankH"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "pointsH"></span></div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-r">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">어웨이에서<br>승점획득비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "pointRankA"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "pointsA"></span></div>
							</div>
						</div>
					</div>
					

					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-l">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">홈에서<br>득점비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "scoredRankH"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "scoredH"></span></div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-r">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">어웨이에서<br>득점비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "scoredRankA"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "scoredA"></span></div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-l">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">홈에서<br>실점비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "concededRankH"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "concededH"></span></div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 homeAdvListItem-r">
							<div class="homeAdvContentDiv">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
										<div class="homeAdvTitle">어웨이에서<br>실점비율</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
										<!-- <div class="homeAdvRank"><span id= "concededRankA"></span>위</div> -->
									</div>
								</div>
								<div class="homeAdvcontent"><span id= "concededA"></span></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="teamRecentMatchDiv row">
					<div>최근 6경기</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 recentMatchListDiv">
						<div id = "teamRecentMatchHtml"></div>
					</div>
				</div>
				
			</div>
			<!-- 요약 END -->
			
			<!-- 골통계 START -->			
			<div class="row secContentDiv swiper-slide" id ="sec01_content">
				<div class="goalMarginDiv row ">
					<div class="subTitleBarDiv"><div class="table-center">경기별 득실차</div></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  table-title-row no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-title-row"><div class="table-center">1골차</div></div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-title-row"><div class="table-center">2골차</div></div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-title-row"><div class="table-center">3골차</div></div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-title-row"><div class="table-center">4골차 이상</div></div>
							</div>
						</div>
					</div>
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="table-title-col"><div class="table-center">홈 승</div></div>
							<div class="table-title-col"><div class="table-center">홈 패</div></div>
							<div class="table-title-col"><div class="table-center">어웨이 승</div></div>
							<div class="table-title-col"><div class="table-center">어웨이 패</div></div>
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "goal1_homeWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal1_homeLose"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal1_awayWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal1_awayLose"></span></div></div>
							</div>
							
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "goal2_homeWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal2_homeLose"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal2_awayWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal2_awayLose"></span></div></div>
							</div>
							
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "goal3_homeWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal3_homeLose"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal3_awayWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal3_awayLose"></span></div></div>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "goal4_homeWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal4_homeLose"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal4_awayWins"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "goal4_awayLose"></span></div></div>
							</div>
						</div>
					</div>
				</div>
					
				<div class="scoringStatsDiv row ">
					<div class="subTitleBarDiv"><div class="table-center">득실차 정보</div></div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="tableTitle"></div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">홈</div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">어웨이</div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">전체</div></div>
							</div>
						</div>	
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="table-title-col"><div class="table-center">클린시트</div></div>
							<div class="table-title-col"><div class="table-center">무실점 승</div></div>
							<div class="table-title-col"><div class="table-center">양팀 득점</div></div>
							<div class="table-title-col"><div class="table-center">득점실패경기</div></div>
							<div class="table-title-col"><div class="table-center">무득점 패</div></div>
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "homeClean"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "homeWonToNil"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "homeBothS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "homeFaliedS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "homeLostToNil"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "awayClean"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayWonToNil"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayBothS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayFaliedS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayLostToNil"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "totalClean"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalWonToNil"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalBothS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalFaliedS"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalLostToNil"></span></div></div>
							</div>
						</div>
					</div>
				</div>
					
				<div class="matchHalfDiv row ">
					<div class="subTitleBarDiv"><div class="table-center">전반전 통계</div></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="tableTitle"></div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">홈</div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">어웨이</div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">전체</div></div>
							</div>
						</div>	
					</div>
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="table-title-col"><div class="table-center">전반 리드</div></div>
							<div class="table-title-col"><div class="table-center">전반 무승부</div></div>
							<div class="table-title-col"><div class="table-center">전반 패배</div></div>
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "leadingHtH"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "drawingHtH"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "losingHtH"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "leadingHtA"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "drawingHtA"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "losingHtA"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "leadingHtT"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "drawingHtT"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "losingHtT"></span></div></div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- 골통계 END -->
			
			<!-- 구단통계 START -->
			<div class="row secContentDiv swiper-slide" id ="sec02_content">
				<div class="gradeAvgDiv row ">
					<div class="subTitleBarDiv"><div class="table-center">홈 & 어웨이 경기</div></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="tableTitle"></div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">전체경기</div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">최근8경기</div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">변화폭</div></div>
							</div>
						</div>	
					</div>
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="table-title-col"><div class="table-center">승점</div></div>
							<div class="table-title-col"><div class="table-center">득점</div></div>
							<div class="table-title-col"><div class="table-center">실점</div></div>
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "DPA"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "DGA"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "DCA"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "DP8"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "DG8"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "DC8"></span></div></div>
							</div>
							
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "PD"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "GD"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "CD"></span></div></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="homeAwayDiv row">
					<div class="subTitleBarDiv"><div class="table-center">홈 & 어웨이 경기</div></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
							<div class="tableTitle"></div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">승</div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">무</div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-title-row"><div class="table-center">패</div></div>
							</div>
						</div>	
					</div>
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
						<div class="table-title-col"><div class="table-center">홈</div></div>
						<div class="table-title-col"><div class="table-center">어웨이</div></div>
						<div class="table-title-col"><div class="table-center">전체</div></div>
						</div>
						
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "homeW"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayW"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalW"></span></div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "homeD"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayD"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalD"></span></div></div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
								<div class="table-content-col"><div class="table-center"><span id= "homeL"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "awayL"></span></div></div>
								<div class="table-content-col"><div class="table-center"><span id= "totalL"></span></div></div>
							</div>
						</div>
					</div>
						
				</div>
			
			</div>
			<!-- 구단통계 END -->
			
			
				
			<div class="row secContentDiv swiper-slide" id ="sec03_content">
				<div class = "content" >
					<div class="TeamAverageDiv row ">
						
						<div class="TeamAverageTab">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItemAvg" id="totalAvgInfo"  onclick = "teamAvgToggle('totalAvgInfo');">전체</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItemAvg" id ="homeAvgInfo" onclick = "teamAvgToggle('homeAvgInfo');">홈</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItemAvg" id ="awayAvgInfo" onclick = "teamAvgToggle('awayAvgInfo');">어웨이</div>
								</div>
							</div>
						</div>
						
						
						<div class= "totalAvgContent" id = "totalAvgInfoDiv">
				    		<!-- 전체 -->
					    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle">홈</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
					    			<div class="teamAdvTitle2">내용</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle3">리그평균</div>
					    		</div>
					    	</div>
					    	
						    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameTT"></span></div>
										<div class="teamAverageItem"><span id= "winsTT"></span></div>
										<div class="teamAverageItem"><span id= "defeatsTT"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameTT"></span></div>
										<div class="teamAverageItem"><span id= "concededGameTT"></span></div>
										<div class="teamAverageItem"><span id= "drawTT"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalTT"></span></div>
										<div class="teamAverageItem"><span id= "over25TT"></span></div>
										<div class="teamAverageItem"><span id= "over35TT"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalTT"></span></div>
									</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem">게임당 평균 승점</div>
										<div class="teamAverageItem">승률</div>
										<div class="teamAverageItem">패배확률</div>
										<div class="teamAverageItem">경기당 평균 득점</div>
										<div class="teamAverageItem">경기당 평균 실점</div>
										<div class="teamAverageItem">비길확률</div>
										<div class="teamAverageItem">경기당 평균 득실점</div>
										<div class="teamAverageItem">득실 > 2.5</div>
										<div class="teamAverageItem">득실 > 3.5</div>
										<div class="teamAverageItem">양팀 득점확률</div>
									</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameTL"></span></div>
										<div class="teamAverageItem"><span id= "winsTL"></span></div>
										<div class="teamAverageItem"><span id= "defeatsTL"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameTL"></span></div>
										<div class="teamAverageItem"><span id= "concededGameTL"></span></div>
										<div class="teamAverageItem"><span id= "drawTL"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalTL"></span></div>
										<div class="teamAverageItem"><span id= "over25TL"></span></div>
										<div class="teamAverageItem"><span id= "over35TL"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalTL"></span></div>
									</div>
					    		</div>
					    	</div>
				    	</div>
				    	
						<div class= "totalAvgContent" id = "homeAvgInfoDiv">		    
						<!-- 홈 --> 
					    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle">홈</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
					    			<div class="teamAdvTitle2">내용</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle3">리그평균</div>
					    		</div>
					    	</div>
									    	
						    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameHT"></span></div>
										<div class="teamAverageItem"><span id= "winsHT"></span></div>
										<div class="teamAverageItem"><span id= "defeatsHT"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameHT"></span></div>
										<div class="teamAverageItem"><span id= "concededGameHT"></span></div>
										<div class="teamAverageItem"><span id= "drawHT"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalHT"></span></div>
										<div class="teamAverageItem"><span id= "over25HT"></span></div>
										<div class="teamAverageItem"><span id= "over35HT"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalHT"></span></div>
									</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem">게임당 평균 승점</div>
										<div class="teamAverageItem">승률</div>
										<div class="teamAverageItem">패배확률</div>
										<div class="teamAverageItem">경기당 평균 득점</div>
										<div class="teamAverageItem">경기당 평균 실점</div>
										<div class="teamAverageItem">비길확률</div>
										<div class="teamAverageItem">경기당 평균 득실점</div>
										<div class="teamAverageItem">득실 > 2.5</div>
										<div class="teamAverageItem">득실 > 3.5</div>
										<div class="teamAverageItem">양팀 득점확률</div>
									</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameHL"></span></div>
										<div class="teamAverageItem"><span id= "winsHL"></span></div>
										<div class="teamAverageItem"><span id= "defeatsHL"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameHL"></span></div>
										<div class="teamAverageItem"><span id= "concededGameHL"></span></div>
										<div class="teamAverageItem"><span id= "drawHL"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalHL"></span></div>
										<div class="teamAverageItem"><span id= "over25HL"></span></div>
										<div class="teamAverageItem"><span id= "over35HL"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalHL"></span></div>
									</div>
					    		</div>
					    	</div>
					    </div>
					    
					    <div class= "totalAvgContent" id = "awayAvgInfoDiv">	
							<!--  어웨이 -->
					    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle">홈</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
					    			<div class="teamAdvTitle2">내용</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
					    			<div class="teamAdvTitle3">리그평균</div>
					    		</div>
					    	</div>
									    	
						    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameAT"></span></div>
										<div class="teamAverageItem"><span id= "winsAT"></span></div>
										<div class="teamAverageItem"><span id= "defeatsAT"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameAT"></span></div>
										<div class="teamAverageItem"><span id= "concededGameAT"></span></div>
										<div class="teamAverageItem"><span id= "drawAT"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalAT"></span></div>
										<div class="teamAverageItem"><span id= "over25AT"></span></div>
										<div class="teamAverageItem"><span id= "over35AT"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalAT"></span></div>
									</div>
					    		</div>
						    	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem">게임당 평균 승점</div>
										<div class="teamAverageItem">승률</div>
										<div class="teamAverageItem">패배확률</div>
										<div class="teamAverageItem">경기당 평균 득점</div>
										<div class="teamAverageItem">경기당 평균 실점</div>
										<div class="teamAverageItem">비길확률</div>
										<div class="teamAverageItem">경기당 평균 득실점</div>
										<div class="teamAverageItem">득실 > 2.5</div>
										<div class="teamAverageItem">득실 > 3.5</div>
										<div class="teamAverageItem">양팀 득점확률</div>
									</div>
					    		</div>
					    		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
									<div class="teamAverageBox">
										<div class="teamAverageItem"><span id= "pointGameAL"></span></div>
										<div class="teamAverageItem"><span id= "winsAL"></span></div>
										<div class="teamAverageItem"><span id= "defeatsAL"></span></div>
										<div class="teamAverageItem"><span id= "scoredGameAL"></span></div>
										<div class="teamAverageItem"><span id= "concededGameAL"></span></div>
										<div class="teamAverageItem"><span id= "drawAL"></span></div>
										<div class="teamAverageItem"><span id= "totalGoalAL"></span></div>
										<div class="teamAverageItem"><span id= "over25AL"></span></div>
										<div class="teamAverageItem"><span id= "over35AL"></span></div>
										<div class="teamAverageItem"><span id= "bothGoalAL"></span></div>
									</div>
					    		</div>
					    	</div>	
				    	
				    	</div>
					</div>
				</div>
			</div>
	
			<!-- 경기정보 START -->
			<div class="row secContentDiv swiper-slide"  id="sec04_content">
				<div class="content">
					<div class="TeamMatchInfoDiv row ">
						
						<div class="matchHistoryTab">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItem" id="allMatch"  onclick = "matchHistorySort('allMatch');">전체</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItem"  id ="afterMatch" onclick = "matchHistorySort('afterMatch');">경기완료</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding table-area">
									<div class="tabItem"  id ="beforeMatch" onclick = "matchHistorySort('beforeMatch');">경기전</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
							<div class ="teamMatchListDiv" id="teamMatchListAllHtml"></div>
							<div class ="teamMatchListDiv" id="teamMatchListBeforeHtml"></div>
							<div class ="teamMatchListDiv" id="teamMatchListAfterHtml"></div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- 경기정보 END -->
			
		</div>
			
			<div class="swiper-pagination"></div>
			
		</div>

</div>
		

		
</body>


<script language="JavaScript">

	var selectTeamId = "${param.teamId}";
	//html 문서
	var matchInfoHtml = "";
	var matchResultHtml = "";
	var rankListHtml = ''; //랭킹
	var teamMatchListHtml = ''; //경기결과 리스트
	
	var teamMatchListAllHtml = '';
	var teamMatchListBeforeHtml = '';
	var teamMatchListAfterHtml = '';

	//Div height default
	var sum ="";
	var windowH =  $(window).outerHeight(true);

	$(window).load(function () {
		//화면세팅
		//setDiv();
	});
	
	$(document).ready(function() {
		
		//팀정보
		getTeamInfo(selectTeamId);
		//팀 요약정보
		getTeamSummeryInfo(selectTeamId);
		//8경기 결과
		getRecentMatchList(selectTeamId);
		//요약정보
		getHomeAverage(selectTeamId);
		//8경기 정보
		getRelativeForm(selectTeamId);
		//match 통계
		getTeamAverage(selectTeamId);
		//득점 통계
		getScoringStats(selectTeamId);
		// 득점 차이 통계
		getGoalMargins(selectTeamId);
		//전반 점수 통계 
		getMatchHalfResult(selectTeamId);
		//경기정보
		getMatchHistory(selectTeamId , "allMatch");
		getMatchHistory(selectTeamId , "afterMatch");
		getMatchHistory(selectTeamId , "beforeMatch");
		
		//vs 리그 default
		teamAvgToggle("totalAvgInfo");
		//경기 default
		matchHistorySort("allMatch");
		//네비 default;
		moveSection();
		//첫화면
		$("#sec00_content").show();
		
		moveSection("frist" ,"init");
		
/* 		$( window ).resize(function() {
			setDiv();
		}); */
		
	    var swiper = new Swiper('.temaInfoContentDiv', {
	         pagination: {
	           el: '.swiper-pagination', 
	           dynamicBullets: true,
	        }, 
	      });
		

	});
	
	function setDiv(){
		var headerH  = (document.getElementById('teamHeader').clientHeight);
		sum = windowH - headerH + "px";
		$("#temaInfoContentDiv").css("height", sum);
		$("#teamheaderDiv").css("height", headerH);
	}
	

	// tab 이동
	function moveSection(obj ,type) {
		
		if(type =="init"){
			$("#sec00").children().show();
			$("#sec00").css("opacity", "1.0");
			$("#sec00").show();
		}else{
			var id = $(obj).attr('id');
			var seq = "#" + id + "_content";
			
			if(id == "sec03"){
				matchHistorySort(type)
			}else if( id =="sec04"){
				matchHistorySort("allMatch");
			}
			
			/* $(".secContentDiv").hide(); */
			$(".navItemTitle").css("opacity", "0.5");
			$(".teamHeaderNavLine").hide();
			$(obj).children().show();
			$(obj).css("opacity", "1.0");
			$(seq).show();
		}
		
	}

	function getTeamInfo(teamId) {

		var formData = new FormData();
		formData.append("teamId", teamId);

		var teamNm, teamshortNm, teamNameEng, director, homeGround, homeTown, foundDate, areaType, rank, img, teamBannerImg;
		var teamInfoHtml ='';

		$.ajax({
			type : 'POST',
			url : "/getTeamInfo",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				//console.log(data.teamInfoList);
                teamBannerImg = data.teamInfoList[0].teamBannerImg;
				teamInfoHtml += '<div class="teamListItem" id ="teamListItem">'
				teamInfoHtml += '<img class="teamBannerImg" src="'+teamBannerImg+'" alt="팀 이미지" ></img>'
				teamInfoHtml += '</div>'
				$('#teamInfoHtml').html(teamInfoHtml);
				
				setDiv();

			},
			error : function(request, status, error) {
			}
		});

	}

	function getRelativeForm(teamId) {

		var formData = new FormData();
		formData.append("teamId", teamId);

		var homeW = '';
		var homeD = '';
		var homeL = '';
		var homeTotal = '';
		var awayW = '';
		var awayD = '';
		var awayL = '';
		var awayTotal = '';
		var totalW = '';
		var totalD = '';
		var totalL = '';
		var resultTotal = '';
		var totalH = '';
		var totalA = '';

		//승점평균
		var DPA = '';
		var DP8 = '';
		var PD = '';

		//득점
		var DGA = '';
		var DG8 = '';
		var GD = '';

		//실점
		var DCA = '';
		var DC8 = '';
		var CD = '';

		$.ajax({
			type : 'POST',
			url : "/getRelative",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {

				//console.log(data.relativeFormList[0]);
				//console.log(data.relativeFormList[1]);

				resultTotal = data.relativeFormList[1].matchCnt;
				homeW = parseInt(data.relativeFormList[1].homeWin);
				homeD = parseInt(data.relativeFormList[1].homeDraw);
				homeL = parseInt(data.relativeFormList[1].homeLose);
				awayW = parseInt(data.relativeFormList[1].awayWin);
				awayD = parseInt(data.relativeFormList[1].awayDraw);
				awayL = parseInt(data.relativeFormList[1].awayLose);
				totalW = homeW + awayW;
				totalD = homeD + awayD;
				totalL = homeL + awayL;
				totalH = homeW + homeD + homeL;
				totalA = awayW + awayD + awayL;

				//평균
				DPA = data.relativeFormList[0].DPA;
				DP8 = data.relativeFormList[0].DP8;
				PD = data.relativeFormList[0].PD;

				//득점
				DGA = data.relativeFormList[0].DGA;
				DG8 = data.relativeFormList[0].DG8;
				GD = data.relativeFormList[0].GD;

				//실점
				DCA = data.relativeFormList[0].DCA;
				DC8 = data.relativeFormList[0].DC8;
				CD = data.relativeFormList[0].CD;

				if (DP8 > 0) {
					$("#DP8").css("color", "#4c9aff");
				} else {
					$("#DP8").css("color", "#ff6678");
				}
				if (PD > 0) {
					$("#PD").css("color", "#4c9aff");
				} else {
					$("#PD").css("color", "#ff6678");
				}

				//평균
				$("#DPA").text(DPA); // 승점/경기수
				$("#DP8").text(DP8); // 8경기 승점/8경기수
				$("#PD").text(PD + "%"); // 변화폭

				//득점
				$("#DGA").text(DGA); // 득점/경기수
				$("#DG8").text(DG8); // 8경기 득점/8경기수
				$("#GD").text(GD + "%"); // 변화폭

				//실점
				$("#DCA").text(DCA); // 실점/경기수
				$("#DC8").text(DC8); // 8경기 실점/8경기수
				$("#CD").text(CD + "%"); // 변화폭

				//홈
				$("#homeW").text(homeW); // 승리
				$("#homeD").text(homeD); // 비김
				$("#homeL").text(homeL); // 패배

				//어웨이
				$("#awayW").text(awayW); // 승리
				$("#awayD").text(awayD); // 비김
				$("#awayL").text(awayL); // 패배

				//전체
				$("#totalW").text(totalW); // 승리
				$("#totalD").text(totalD); // 비김
				$("#totalL").text(totalL); // 패배

			},
			error : function(request, status, error) {
			}
		});

	}


	function getTeamAverage(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);

		var totalAreaT, totalAreaL, homeAreaT, homeAreaL, awayAreaT, awayAreaL;
		var bothGoal ,concededGame, defeats, draw, over25, over35, pointGame, scoredGame, teamID, totalGoal, wins;

		$.ajax({
			type : 'POST',
			url : "/getTeamAverage",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				//console.log(data.teamAverageList);
				//전체
				totalAreaT = data.teamAverageList[4];
				totalAreaL = data.teamAverageList[5];
				//홈
				homeAreaT = data.teamAverageList[0];
				homeAreaL = data.teamAverageList[1];
				//어웨이
				awayAreaT = data.teamAverageList[2];
				awayAreaL = data.teamAverageList[3];

				//total
				$("#pointGameTT").text(totalAreaT.pointGame);
				$("#winsTT").text(totalAreaT.wins + "%");
				$("#defeatsTT").text(totalAreaT.defeats + "%");
				$("#scoredGameTT").text(totalAreaT.scoredGame);
				$("#concededGameTT").text(totalAreaT.concededGame);
				$("#totalGoalTT").text(totalAreaT.totalGoal);
				$("#drawTT").text(totalAreaT.draw + "%");
				$("#over25TT").text(totalAreaT.over25 + "%");
				$("#over35TT").text(totalAreaT.over35 + "%");
				$("#bothGoalTT").text(totalAreaT.bothGoal + "%");

				$("#pointGameTL").text(totalAreaL.pointGame);
				$("#winsTL").text(totalAreaL.wins + "%");
				$("#defeatsTL").text(totalAreaL.defeats + "%");
				$("#scoredGameTL").text(totalAreaL.scoredGame);
				$("#concededGameTL").text(totalAreaL.concededGame);
				$("#totalGoalTL").text(totalAreaL.totalGoal);
				$("#drawTL").text(totalAreaL.draw + "%");
				$("#over25TL").text(totalAreaL.over25 + "%");
				$("#over35TL").text(totalAreaL.over35 + "%");
				$("#bothGoalTL").text(totalAreaL.bothGoal + "%");

				//home
				$("#pointGameHT").text(homeAreaT.pointGame);
				$("#winsHT").text(homeAreaT.wins + "%");
				$("#defeatsHT").text(homeAreaT.defeats + "%");
				$("#scoredGameHT").text(homeAreaT.scoredGame);
				$("#concededGameHT").text(homeAreaT.concededGame);
				$("#totalGoalHT").text(homeAreaT.totalGoal);
				$("#drawHT").text(homeAreaT.draw + "%");
				$("#over25HT").text(homeAreaT.over25 + "%");
				$("#over35HT").text(homeAreaT.over35 + "%");
				$("#bothGoalHT").text(homeAreaT.bothGoal + "%");

				$("#pointGameHL").text(homeAreaL.pointGame);
				$("#winsHL").text(homeAreaL.wins + "%");
				$("#defeatsHL").text(homeAreaL.defeats + "%");
				$("#scoredGameHL").text(homeAreaL.scoredGame);
				$("#concededGameHL").text(homeAreaL.concededGame);
				$("#totalGoalHL").text(homeAreaL.totalGoal);
				$("#drawHL").text(homeAreaL.draw + "%");
				$("#over25HL").text(homeAreaL.over25 + "%");
				$("#over35HL").text(homeAreaL.over35 + "%");
				$("#bothGoalHL").text(homeAreaL.bothGoal + "%");

				//away
				$("#pointGameAT").text(awayAreaT.pointGame);
				$("#winsAT").text(awayAreaT.wins + "%");
				$("#defeatsAT").text(awayAreaT.defeats + "%");
				$("#scoredGameAT").text(awayAreaT.scoredGame);
				$("#concededGameAT").text(awayAreaT.concededGame);
				$("#totalGoalAT").text(awayAreaT.totalGoal);
				$("#drawAT").text(awayAreaT.draw + "%");
				$("#over25AT").text(awayAreaT.over25 + "%");
				$("#over35AT").text(awayAreaT.over35 + "%");
				$("#bothGoalAT").text(awayAreaT.bothGoal + "%");

				$("#pointGameAL").text(awayAreaL.pointGame);
				$("#winsAL").text(awayAreaL.wins + "%");
				$("#defeatsAL").text(awayAreaL.defeats + "%");
				$("#scoredGameAL").text(awayAreaL.scoredGame);
				$("#concededGameAL").text(awayAreaL.concededGame);
				$("#totalGoalAL").text(awayAreaL.totalGoal);
				$("#drawAL").text(awayAreaL.draw + "%");
				$("#over25AL").text(awayAreaL.over25 + "%");
				$("#over35AL").text(awayAreaL.over35 + "%");
				$("#bothGoalAL").text(awayAreaL.bothGoal + "%");

			},
			error : function(request, status, error) {
			}
		});
	}

	function getHomeAverage(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);
		$.ajax({
			type : 'POST',
			url : "/getHomeAdvantage",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				//console.log(data.homeAdvantageList);
				$.each(data.homeAdvantageList, function(idx, val) {
					$("#pointsH").text(val.pointH + "%");
					$("#pointsA").text(val.pointA + "%");
					$("#pointRankH").text(val.pointRankH);
					$("#pointRankA").text(val.pointRankA);

					$("#ppgh").text(val.ppgh + "P");
					$("#ppga").text(val.ppga + "P");
					$("#ppghRank").text(val.ppghRank);
					$("#ppgaRank").text(val.ppgaRank);

					$("#scoredH").text(val.scoredH + "%");
					$("#scoredA").text(val.scoredA + "%");
					$("#scoredRankH").text(val.scoredRankH);
					$("#scoredRankA").text(val.scoredRankA);

					$("#concededH").text(val.concededH + "%");
					$("#concededA").text(val.concededA + "%");
					$("#concededRankH").text(val.concededRankH);
					$("#concededRankA").text(val.concededRankA);
				});
			},
			error : function(request, status, error) {
			}
		});
	}
	
	
	

	function getScoringStats(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);

		//클린시트
		var homeClean, awayClean, totalClean;
		//무실점 승
		var homeWonToNil, awayWonToNil, totalWonToNil;
		// 양팀득점
		var homeBothS, awayBothS, totalBothS;
		//득점실패경기
		var homeFaliedS, awayFaliedS, totalFaliedS;
		//무득점 패
		var homeLostToNil, awayLostToNil, totalLostToNil;

		$.ajax({
			type : 'POST',
			url : "/getScoringStats",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {

				$("#homeClean").text(data.scoringStatsList[0].num + "%");
				$("#awayClean").text(data.scoringStatsList[1].num + "%");
				$("#totalClean").text(data.scoringStatsList[2].num + "%");

				$("#homeWonToNil").text(data.scoringStatsList[3].num + "%");
				$("#awayWonToNil").text(data.scoringStatsList[4].num + "%");
				$("#totalWonToNil").text(data.scoringStatsList[5].num + "%");

				$("#homeBothS").text(data.scoringStatsList[6].num + "%");
				$("#awayBothS").text(data.scoringStatsList[7].num + "%");
				$("#totalBothS").text(data.scoringStatsList[8].num + "%");

				$("#homeFaliedS").text(data.scoringStatsList[9].num + "%");
				$("#awayFaliedS").text(data.scoringStatsList[10].num + "%");
				$("#totalFaliedS").text(data.scoringStatsList[11].num + "%");

				$("#homeLostToNil").text(data.scoringStatsList[12].num + "%");
				$("#awayLostToNil").text(data.scoringStatsList[13].num + "%");
				$("#totalLostToNil").text(data.scoringStatsList[14].num + "%");
			},
			error : function(request, status, error) {
			}
		});
	}

	function getGoalMargins(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);

		$.ajax({
			type : 'POST',
			url : "/getGoalMargins",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				/* console.log(data.goalMarginsList); */
				//1경기 
				$("#goal1_homeWins").text(data.goalMarginsList.goal1[0].num);
				$("#goal1_homeLose").text(data.goalMarginsList.goal1[1].num);
				$("#goal1_awayWins").text(data.goalMarginsList.goal1[2].num);
				$("#goal1_awayLose").text(data.goalMarginsList.goal1[3].num);

				//2경기 
				$("#goal2_homeWins").text(data.goalMarginsList.goal2[0].num);
				$("#goal2_homeLose").text(data.goalMarginsList.goal2[1].num);
				$("#goal2_awayWins").text(data.goalMarginsList.goal2[2].num);
				$("#goal2_awayLose").text(data.goalMarginsList.goal2[3].num);

				//3경기 
				$("#goal3_homeWins").text(data.goalMarginsList.goal3[0].num);
				$("#goal3_homeLose").text(data.goalMarginsList.goal3[1].num);
				$("#goal3_awayWins").text(data.goalMarginsList.goal3[2].num);
				$("#goal3_awayLose").text(data.goalMarginsList.goal3[3].num);

				//4경기 
				$("#goal4_homeWins").text(data.goalMarginsList.goal4[0].num);
				$("#goal4_homeLose").text(data.goalMarginsList.goal4[1].num);
				$("#goal4_awayWins").text(data.goalMarginsList.goal4[2].num);
				$("#goal4_awayLose").text(data.goalMarginsList.goal4[3].num);
			},
			error : function(request, status, error) {
			}
		});
	}

	function getMatchHalfResult(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);

		var leadingHtH, drawingHtH, losingHtH;
		var leadingHtA, drawingHtH, losingHtH;
		var totalMatch;

		$.ajax({
			type : 'POST',
			url : "/getMatchHalfResult",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {

				leadingHtH = parseInt(data.matchHalfResult[0].num);
				drawingHtH = parseInt(data.matchHalfResult[1].num);
				losingHtH = parseInt(data.matchHalfResult[2].num);

				leadingHtA = parseInt(data.matchHalfResult[3].num);
				drawingHtA = parseInt(data.matchHalfResult[4].num);
				losingHtA = parseInt(data.matchHalfResult[5].num);

				totalMatch = leadingHtH + drawingHtH + losingHtH + leadingHtA + drawingHtA + losingHtA;

				leadingHtT = Math.round((leadingHtH + leadingHtA) / totalMatch * 100, 0) + "%";
				drawingHtT = Math.round((drawingHtH + drawingHtA) / totalMatch * 100, 0) + "%";
				losingHtT = Math.round((losingHtH + losingHtA) / totalMatch * 100, 0)+ "%";

				$("#leadingHtH").text(data.matchHalfResult[0].num);
				$("#drawingHtH").text(data.matchHalfResult[1].num);
				$("#losingHtH").text(data.matchHalfResult[2].num);

				$("#leadingHtA").text(data.matchHalfResult[3].num);
				$("#drawingHtA").text(data.matchHalfResult[4].num);
				$("#losingHtA").text(data.matchHalfResult[5].num);

				$("#leadingHtT").text(leadingHtT);
				$("#drawingHtT").text(drawingHtT);
				$("#losingHtT").text(losingHtT);

			},
			error : function(request, status, error) {
			}
		});
	}

	/* 요약정보 */
	function getTeamSummeryInfo(teamId) {
		var formData = new FormData();
		formData.append("teamId", teamId);
		var rank , goalRank, gapRank, winPer;
		
		$.ajax({
			type : 'POST',
			url : "/getTeamRank",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				//console.log(data.teamRankList);
				rank = data.teamRankList[0].rank;
				goalRank = data.teamRankList[0].goalRank;
				gapRank = data.teamRankList[0].gapRank;
				winPer = data.teamRankList[0].winPer;
				
				$("#rank").text(data.teamRankList[0].rank + "위");
				$("#goalRank").text(data.teamRankList[0].goalRank + "위");
				$("#gapRank").text(data.teamRankList[0].gapRank + "위");
				$("#winPer").text(data.teamRankList[0].winPer + "%");
			},
			error : function(request, status, error) {
			}
		});
	}

	
	//경기정보생성
	function getMatchHistory(teamId, type) {
		var formData = new FormData();
		formData.append("teamId", teamId);
		var htmlType = '';
		
		if(type =="allMatch"){
			formData.append("sortType", "dateAsc");
			htmlType = "teamMatchListAllHtml";
		}else if(type =="afterMatch"){
			formData.append("state", "after");
			formData.append("sortType", "dateDesc");
			htmlType = "teamMatchListBeforeHtml";
		}else if(type =="beforeMatch"){
			formData.append("state", "before");
			formData.append("sortType", "dateAsc");
			htmlType = "teamMatchListAfterHtml";
		}
		
  		var HomeTeam, HomeTeamId, AwayTeam, AwayTeamId, homeScore, awayScore, fristday, lastday, matchDate ,matchGround,homeTeamImg, awayTeamImg, htHome, htAway;

		$.ajax({
			type : 'POST',
			url : "/getMatchHistory",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				$.each(data.matchHistoryList, function(idx, val) {
	 				HomeTeam = val.homeShortNm;
	 				HomeTeamId = val.homeTeamId;
	 				AwayTeam = val.awayShortNm;
	 				AwayTeamId = val.awayTeamId;
	 				homeScore = val.homeScore;
	 				awayScore = val.awayScore;
	 				matchDate = val.matchDate;
	 				matchGround = val.matchGround;
	 				homeTeamImg = val.homeTeamImg;
	 				awayTeamImg = val.awayTeamImg;
	 				
					makeTeamMatchListHtml(HomeTeamId,AwayTeamId,HomeTeam,AwayTeam,homeScore,awayScore,matchDate,matchGround,homeTeamImg,awayTeamImg,htmlType);
				});
			},
			error : function(request, status, error) {
			}
		});
	}


	//경기결과 노출
	function makeTeamMatchListHtml(HomeTeamId, AwayTeamId, HomeTeam, AwayTeam,homeScore, awayScore, matchDate, matchGround, homeTeamImg,awayTeamImg , htmlType) {

		var htmlId = "#"+ htmlType;
		var matchDate = matchDate.substring(2, 4) + "년 "+ matchDate.substring(4, 6) + "월 " + matchDate.substring(6, 8)+ "일 " + matchDate.substring(8, 10) + "시 "
				+ matchDate.substring(10, 12) + "분 ";

		var matchTimeInfo = '';
		var scoureInfo = '';

		if (homeScore == '' || typeof (homeScore) == "undefined") {
			scoureInfo = '<div class="matchScoure">VS</div>';
			matchTimeInfo = "경기전";
		} else {
			scoureInfo = '<div class="matchScoure">' + homeScore + " : "+ awayScore + '</div>';
			matchTimeInfo = "경기종료";
		}
	
		$(htmlId).append(
		 		'<div class = "matchList">'
		 			+ '<div class="matchInfoDiv">'
		 			+'		<div class="matchDay">' + matchDate + '</div>'
		 			+ '		<div class="matchGround">' + matchGround + '</div>'
		 			+ '</div>'
		 			+ '<div class="matchTeamInfoDiv">'
		 			+ '		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
		 			 + '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		 			 + '				<div class="matchTeamItem" onclick="detailTeam(this);" id= "'+ HomeTeamId + '">'
		 			 + '				<div><img class="teamLogoImg" src="'+homeTeamImg+'" ></img></div>'
		 			 + '				<div class="matchTeamName">' + HomeTeam + '</div>'
		 			 + '			</div>'
		 			 + '			</div>'
		 			 + '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		 			 + '				<div class="matchTeamItem">'
		 			 +                      scoureInfo
		 			 + '					<div class="matchResult">' + matchTimeInfo + '</div>'
		 			 + '				</div>'
		 			 + '			</div>'
		 			 + '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		 			 + '				<div class="matchTeamItem" onclick="detailTeam(this);" id= "'+ AwayTeamId + '">'
		 			 + '					<div><img class="teamLogoImg" src="'+awayTeamImg+'" ></img></div>'
		 			 + '					<div class="matchTeamName">' + AwayTeam + '</div>'
		 			 + '				</div>'
		 			 + '			</div>'
		 			 + '		</div>'
		 			 + '	</div>'
		 			 + '</div>'
				); 

	}

	function getRecentMatchList(teamId) {

		var formData = new FormData();
		formData.append("teamId", teamId);
		formData.append("sortType", "dateDesc");
		formData.append("limitCnt", "6");
		formData.append("state", "after");

		var teamRecentMatchHtml = '';

		$.ajax({
				type : 'POST',
				url : "/getMatchHistory",
				processData : false,
				contentType : false,
				data : formData,
				success : function(data, status) {
					$.each(data.matchHistoryList, function(idx, val) {
		
						teamRecentMatchHtml += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 recentMatchList">'
						if (val.result == "승") {
							teamRecentMatchHtml += '<div class="recentMatchItem-W">'+ val.result + '</div>'
						} else if (val.result == "패") {
							teamRecentMatchHtml += '<div class="recentMatchItem-L">'+ val.result + '</div>'
						} else {
							teamRecentMatchHtml += '<div class="recentMatchItem-D">'+ val.result + '</div>'
						}
						teamRecentMatchHtml += '</div>'
					});

					$('#teamRecentMatchHtml').html(teamRecentMatchHtml);
				},
					error : function(request, status, error) {
					}
				});
	}

	//경기정보 정렬 
	function matchHistorySort(type) {
		$(".tabItem").css("background", "#fff").css("color", "#000");
		$("#" + type).css("background", "#000").css("color", "#fff");
		$(".teamMatchListDiv").hide();
		if (type == "allMatch") {
			$("#teamMatchListAllHtml").show();
		} else if (type == "afterMatch") {
			$("#teamMatchListBeforeHtml").show();
		} else {
			$("#teamMatchListAfterHtml").show();
		}
	}

	//vs통계 nav
	function teamAvgToggle(type) {
		$(".tabItemAvg").css("background", "#fff").css("color", "#000");
		$("#" + type).css("background", "#000").css("color", "#fff");
		$(".totalAvgContent").hide();
		$("#" + type + "Div").show();
	}
</script>

</html>
