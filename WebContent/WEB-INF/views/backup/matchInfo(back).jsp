<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>



<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link href="/resources/css/micro.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
</head>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>

	<div class="container imgBackGround">
		<div class="row rankHeaderDiv" id ="rankHeaderDiv">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
<!-- 				<div class="headerImg" id = "headerDiv">
					<img class="logoImg" src="/resources/images/premierLogo.png" ></img>
				</div> -->
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
					<div class= "leagueTitleEng">Premier League</div>
					
					<div class= "leagueTitleKor">프리미어리그</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="roundInfoDiv" id = "roundInfoDiv">
<!-- 					<div class="roundDay"><span id="matchDay"></span></div> -->
				<div class="matchCnt">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
							<button class="paginate left"><i></i><i></i></button>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
							<div class="counter"></div>
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding">
							<button class="paginate right"><i></i><i></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>	
			
		<div class="row">
			<div class= "matchResultDiv" id ="matchInfoHtml"></div>
		</div> 
			
		<div class="row" >
			<div class="navBarDiv" id = "navBarDiv">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						
						<div class="navbarItem" id="rank" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="rankLine" style="display: none;"></div>순위</div>
						<div class="navbarItem" id="rankImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_rank.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="match" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="matchLine" style="display: none;"></div>경기</div>
						<div class="navbarItem" id="matchImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_match.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="team" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="teamLine" style="display: none;"></div>구단</div>
						<div class="navbarItem" id="teamImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_team.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="noti"  onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="notiLine" style="display: none;"></div>공지</div>
						<div class="navbarItem" id="notiImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_notice.png" ></img>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>


<script language="JavaScript">
	
	var matchInfoHtml = '';
	
	var matchDay;
	var matchRound;
	var maxRound;
  	var index = 1; // 매치 first
  	var total = 36; // 매치 last
	
  	
	//Div height default
	var sum ="";
	var windowH =  $(window).outerHeight(true);
	var rankHeaderDivH =  $("#rankHeaderDiv").outerHeight(true);
	var roundInfoH =  $("#roundInfoDiv").outerHeight(true);
	var navBarDivH =  $("#navBarDiv").outerHeight(true);
 	
	var pr = document.querySelector( '.paginate.left' );
	var pl = document.querySelector( '.paginate.right' );
	
	$(document).ready( function(){

		toggleNavCss("matchInfo"); // navTab 세팅
  		setDiv();//div Settring
		
		getMatchRound(); //최근라운드 가저오기
  	  	
		pr.onclick = slide.bind( this, -1 );
  	  	pl.onclick = slide.bind( this, 1 );
  	  	
  		
	});
	
	function setDiv(){
		sum = windowH - rankHeaderDivH - roundInfoH -navBarDivH + "px";
		$("#matchInfoHtml").css("height", sum);
	}
  	
	/* btn-slide START */
	// 매치라운드 slide
	function slide(offset) {

	  index = Math.min( Math.max( index + offset, 1 ), total );
	  document.querySelector( '.counter' ).innerHTML = index + 'R';

	  pr.setAttribute( 'data-state', index === 1 ? 'disabled' : '' );
	  pl.setAttribute( 'data-state', index === total ? 'disabled' : '' );
	  
	  matchInfoHtml = '';
	  
	  getWeekMatchList(index);
	}
	
	function toggleNavCss(urlHtml){
		 //var type ="";
		 //type =  window.location.toString().includes(urlHtml); 
  		 $("#matchLine").show();
  		 $("#match").show();
  		 
		 $("#rankImg").show();
		 $("#rankImg").css("opacity","0.5");
		 $("#teamImg").show();
		 $("#teamImg").css("opacity","0.5");
		 $("#notiImg").show();
		 $("#notiImg").css("opacity","0.5");
		 
	}
	
	//매치라운드 가저오기
	function getMatchRound(){
		$.ajax({
			type : 'POST',
			url : "/getMatchRound",
			processData : false,
			contentType : false,
			success : function(data, status) {
				matchRound = parseInt(data.matchRound[0].nowMatchRound);
				setMatchRound = matchRound - 1;
				total = data.matchRound[0].maxRound;
				//초기값 설정 (라운드 -1 );
				slide(setMatchRound);
			},
			error : function(request, status, error) {
			}
		});
	}
	
	function getWeekMatchList(matchRound){
		var formData = new FormData();
		formData.append("matchRound", matchRound);
		
  		var HomeTeam;
  		var HomeTeamId;
  		var AwayTeam;
  		var AwayTeamId;
		var homeScore;
		var awayScore;	
		var fristday;
		var lastday;
		var matchDate;
		var matchGround;
		var homeTeamImg;
		var awayTeamImg;
		var stat;
		
		$.ajax({
			type : 'POST',
			url : "/getWeekMatchList",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				matchInfoHtml = '';
				
				fristday = data.weekMatchList[0].fristday;
				lastday = data.weekMatchList[0].lastday;
				maxRound = data.weekMatchList[0].maxRound;
				matchDay = fristday.substring(0, 4)+"."+fristday.substring( 4, 6 )+"."+fristday.substring( 6, 8 )+" - "+lastday.substring(0, 4)+"."+lastday.substring( 4, 6 )+"."+lastday.substring( 6, 8 );
				
				$("#matchDay").text(matchDay);
				
				console.table(data.weekMatchList);
				
				$.each(data.weekMatchList, function(idx, val) {
	 				HomeTeam = val.homeShortNm;
	 				HomeTeamId = val.homeTeamId;
	 				AwayTeam = val.awayShortNm;
	 				AwayTeamId = val.awayTeamId;
	 				homeScore = val.homeScore;
	 				awayScore = val.awayScore;
	 				matchDate = val.matchDate;
	 				matchGround = val.matchGround;
	 				homeTeamImg = val.homeTeamImg;
	 				awayTeamImg = val.awayTeamImg;
	 				stat = val.stat;
	 				
	 				makeWeekMatchResultHtml(HomeTeamId,AwayTeamId,HomeTeam,AwayTeam,homeScore,awayScore,matchDate,matchGround,homeTeamImg,awayTeamImg,stat);
				});
				
			},
			error : function(request, status, error) {
			}
		});
  	}
	
	//경기결과 노출
	function makeWeekMatchResultHtml(HomeTeamId,AwayTeamId,HomeTeam,AwayTeam,homeScore,awayScore,matchDate,matchGround,homeTeamImg,awayTeamImg,stat){
		
		var matchDate = matchDate.substring(2, 4)+"년 "+matchDate.substring(4, 6)+"월 "+matchDate.substring(6, 8)+"일 "+ matchDate.substring(8, 10)+"시 "+matchDate.substring(10, 12)+"분 ";
		
		
/* 		if(matchDate >= getTimeStamp()){
		}else{
			matchTimeInfo = "경기종료";
		} */
		
		var matchTimeInfo = '';
		var scoureInfo ='';
		
		if(homeScore == '' || typeof(homeScore) =="undefined"){
			scoureInfo = '<div class="matchScoure">VS</div>';
			matchTimeInfo = "경기전";
		}else{
			scoureInfo = '<div class="matchScoure">'+homeScore+" : "+awayScore+'</div>';
			matchTimeInfo = "경기종료";
		}
		
		matchInfoHtml += '<div class = "matchList">'
		matchInfoHtml += '	<div class="matchInfoDiv">'
		matchInfoHtml += '		<div class="matchDay">'+matchDate+'</div>'
		matchInfoHtml += '		<div class="matchGround">'+matchGround+'</div>'
		matchInfoHtml += '	</div>'
		matchInfoHtml += '	<div class="matchTeamInfoDiv">'
		matchInfoHtml += '		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
		matchInfoHtml += '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		matchInfoHtml += '				<div class="matchTeamItem" onclick="detailTeam(this);" id= "'+HomeTeamId +'">'
		matchInfoHtml += '				<div><img class="teamLogoImg" src="'+homeTeamImg+'" ></img></div>'
		matchInfoHtml += '				<div class="matchTeamName">'+HomeTeam+'</div>'
		matchInfoHtml += '			</div>'
		matchInfoHtml += '			</div>'
		matchInfoHtml += '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		matchInfoHtml += '				<div class="matchTeamItem">'
		matchInfoHtml += 					scoureInfo
		matchInfoHtml += '					<div class="matchResult">'+matchTimeInfo+'</div>'
		matchInfoHtml += '				</div>'
		matchInfoHtml += '			</div>'
		matchInfoHtml += '			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">'
		matchInfoHtml += '				<div class="matchTeamItem" onclick="detailTeam(this);" id= "'+AwayTeamId +'">'
		matchInfoHtml += '					<div><img class="teamLogoImg" src="'+awayTeamImg+'" ></img></div>'
		matchInfoHtml += '					<div class="matchTeamName">'+AwayTeam+'</div>'
		matchInfoHtml += '				</div>'
		matchInfoHtml += '			</div>'
		matchInfoHtml += '		</div>'
		matchInfoHtml += '	</div>'
		matchInfoHtml += '</div>'
		$('#matchInfoHtml').html(matchInfoHtml);
	}
	
	//구단 상세정보 페이지 
	function detailTeam(obj){
		var id = $(obj).attr('id'); //id
		location.href ="/teamInfo?teamId="+id;
	}
	
	//url 이동
	function goUrl(obj) {
		var type = $(obj).attr('id');
		console.log(type);
		if (type == "rank" || type == "rankImg") {
			location.href = "/teamRank";
		} else if (type == "match" || type == "matchImg") {
			location.href = "/matchInfo";
		} else if (type == "team" || type == "teamImg") {
			location.href = "/teamList";
		} else {
			location.href = "/notice";
		}
	}
	
	function getTimeStamp() {
		  var d = new Date();
		  var s =
		    leadingZeros(d.getFullYear(), 4) +
		    leadingZeros(d.getMonth() + 1, 2)+
		    leadingZeros(d.getDate(), 2)+
		    leadingZeros(d.getHours(), 2)+
		    leadingZeros(d.getMinutes(), 2);
		  return s;
		}

		function leadingZeros(n, digits) {
		  var zero = '';
		  n = n.toString();

		  if (n.length < digits) {
		    for (i = 0; i < digits - n.length; i++)
		      zero += '0';
		  }
		  return zero + n;
		}
	
</script>

</html>
