<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
	
	<title>KICKKICK</title>
	
</head>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>

	<div class="container imgBackGround">
		
		<div class="row">
			<div class="teamListDiv" id = "teamListDiv">
				<div id ="teamListHtml"></div>
			</div>
		</div>
		
		<div class="row" >
			<div class="navBarDiv" id = "navBarDiv">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						
						<div class="navbarItem" id="rank" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="rankLine" style="display: none;"></div>순위</div>
						<div class="navbarItem" id="rankImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_rank.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="match" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="matchLine" style="display: none;"></div>경기</div>
						<div class="navbarItem" id="matchImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_match.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="team" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="teamLine" style="display: none;"></div>구단</div>
						<div class="navbarItem" id="teamImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_team.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="noti"  onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="notiLine" style="display: none;"></div>공지</div>
						<div class="navbarItem" id="notiImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_notice.png" ></img>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</body>

<script>
	
	var teamListHtml = '';
	
	//Div height default
	var sum ="";
	var windowH =  $(window).outerHeight(true);
	var navBarDivH =  $("#navBarDiv").outerHeight(true);
	
	$( document ).ready( function(){
		//rankList Set
		getTeamListInfo();
		//tab Setting
		toggleNavCss("team");
		//div Settring
		setDiv();
	});
	
	function setDiv(){
		sum = windowH - navBarDivH + "px";
		$("#teamListDiv").css("height", sum);
	}
	
	function getTeamListInfo() {

		var formData = new FormData();
		formData.append("areaType", "E"); 
		var teamId, teamNm, teamshortNm, teamNameEng, director, homeGround, homeTown, foundDate, areaType, rank, img, teamBannerImg;

		$.ajax({
			type : 'POST',
			url : "/getTeamInfo",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				/* console.table(data.teamInfoList); */

				$.each(data.teamInfoList, function(idx, val) {
					teamId = val.teamId;
					teamName = val.teamName;
					teamshortNm = val.teamshortNm;
					teamNameEng = val.teamNameEng;
					director = val.director;
					homeGround = val.homeGround;
					homeTown = val.homeTown;
					foundDate = val.foundDate;
					areaType = val.areaType;
					rank = val.rank;
					teamBannerImg = val.teamBannerImg;

					maketeamListHtml(teamId, teamName, teamshortNm,
							teamNameEng, director, homeGround, homeTown,
							foundDate, areaType, rank, img, teamBannerImg);
				});

			},
			error : function(request, status, error) {
			}
		});

	}
	
	//구단 상세정보 페이지 
	function detailTeam(obj){
		var id = $(obj).attr('id'); //id
		location.href ="/teamInfo?teamId="+id;
	}

	//경기결과 노출
	function maketeamListHtml(teamId, teamName, teamshortNm, teamNameEng,
			director, homeGround, homeTown, foundDate, areaType, rank, img,
			teamBannerImg) {
		teamListHtml += '<div class="teamListItem" onclick="detailTeam(this);" id = "'+teamId+'">'
		teamListHtml += '<img class="teamBannerImg" src="'+teamBannerImg+'" alt="팀 이미지" ></img>'
		teamListHtml += '</div>'
		$('#teamListHtml').html(teamListHtml);
	}

	function toggleNavCss(urlHtml) {
		$("#teamLine").show();
		$("#team").show();

		$("#rankImg").show();
		$("#rankImg").css("opacity", "0.5");
		$("#matchImg").show();
		$("#matchImg").css("opacity", "0.5");
		$("#notiImg").show();
		$("#notiImg").css("opacity", "0.5");

	}

	//url 이동
	function goUrl(obj) {
		var type = $(obj).attr('id');
		console.log(type);
		if (type == "rank" || type == "rankImg") {
			location.href = "/teamRank";
		} else if (type == "match" || type == "matchImg") {
			location.href = "/matchInfo";
		} else if (type == "team" || type == "teamImg") {
			location.href = "/teamList";
		} else {
			location.href = "/notice";
		}
	}
</script>

</html>
