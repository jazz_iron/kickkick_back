<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
	
	<title>KICKKICK</title>
	
</head>

<style>

/* Basic Reset */

/* Structure */

*,
*:before,
*:after {
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}


/* Structure */

/* body {
  font: 87.5%/1.389em 'Exo 2', sans-serif;
  color: #000;
  background: #222;
} */

.noticeContent{
	overflow: auto;
}

h2, p { margin-bottom: 1em; }

.toggle {
  position: fixed;
  top: 20px; right: 20px;
  z-index: 10;
  box-shadow: 0 0 2px rgba(0,0,0,0.2);
}

.toggle a {
  position: relative;
  float: left; display: block;
  width: 32px; height: 32px;
  text-indent: -9999px;
  background: #222;
  box-shadow: inset 0 0 0 2px #eee;
  cursor: pointer;
}
.toggle a.active {
  background: #eee;
  cursor: default;
}

.toggle .skew { border-radius: 3px 0 0 3px; }

.tiltlist {
    list-style: none;
    position: relative;
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
    padding: 0px 15px;
}

.tiltlist:before,
.tiltlist:after {
  content: '';
  position: absolute;
  display: block;
  width: 100%; 
  height: 50px;
  margin-left: -15px;
  background: #eee;
} 

.tiltlist:before { top: -50px; margin: 0px; }
.tiltlist:after { bottom: -50px; right: 15px; }

.tiltlist li {
   padding: 2em 3em 1em;
   margin: 10px 0px;
   width: 100%;
   background: #eee;
   transition: transform 250ms;
}

.tiltlist.skew:before,
.tiltlist.skew:after,
.tiltlist.skew li { transform: skewY(-8deg); }

/* Theme */

.tiltlist li:nth-child(2n) h2 { color: #00b5d9; }
.tiltlist li:nth-child(2n+1) h2 { color: #ff5454; }

</style>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>

	<div class="container imgBackGround">
		
		<div class="row noticeContent" id ="noticeContent">
			<ul class="tiltlist skew">
			  <li>
			    <h2>형님들</h2>
			    <p>서버비용 부탁드립니다. kickkick@gmail.com</p>
			  </li>
			  <li>
			    <h2>Lorem ipsum</h2>
			    <p>Dolor sit amet, consectetur adipisicing elit. Nesciunt id tempore sit modi accusamus sequi velit consequuntur iusto temporibus nam.</p>
			  </li>
			  <li>
			    <h2>Lorem ipsum</h2>
			    <p>Dolor sit amet, consectetur adipisicing elit. Nesciunt id tempore sit modi accusamus sequi velit consequuntur iusto temporibus nam.</p>
			  </li>
			  <li>
			    <h2>Lorem ipsum</h2>
			    <p>Dolor sit amet, consectetur adipisicing elit. Nesciunt id tempore sit modi accusamus sequi velit consequuntur iusto temporibus nam.</p>
			  </li>
			  <li>
			    <h2>Lorem ipsum</h2>
			    <p>Dolor sit amet, consectetur adipisicing elit. Nesciunt id tempore sit modi accusamus sequi velit consequuntur iusto temporibus nam.</p>
			  </li>
			  <li>
			    <h2>Lorem ipsum</h2>
			    <p>Dolor sit amet, consectetur adipisicing elit. Nesciunt id tempore sit modi accusamus sequi velit consequuntur iusto temporibus nam.</p>
			  </li>
			</ul>
		
		</div>
		
		<div class="row" >
			<div class="navBarDiv" id = "navBarDiv">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						
						<div class="navbarItem" id="rank" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="rankLine" style="display: none;"></div>순위</div>
						<div class="navbarItem" id="rankImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_rank.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="match" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="matchLine" style="display: none;"></div>경기</div>
						<div class="navbarItem" id="matchImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_match.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="team" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="teamLine" style="display: none;"></div>구단</div>
						<div class="navbarItem" id="teamImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_team.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="noti"  onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="notiLine" style="display: none;"></div>공지</div>
						<div class="navbarItem" id="notiImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_notice.png" ></img>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</body>

<script>
	
	//Div height default
	var sum ="";
	var windowH =  $(window).outerHeight(true);
	var navBarDivH =  $("#navBarDiv").outerHeight(true);
	
	$( document ).ready( function(){
		//tab Setting
		toggleNavCss("noti");
		//div Settring
		setDiv();
		
		var toggle = $('.toggle'),
	    tiltlist = $('.tiltlist');

	toggle.find('a').on('click', function() {
	  var self = $(this); self.addClass('active').siblings('.active').removeClass('active');
	  tiltlist.attr('class', 'tiltlist ' + self.attr('class'));
	});
		
	});
	
	function setDiv(){
		sum = windowH - navBarDivH + "px";
		$("#noticeContent").css("height", sum);
	}
	
	function toggleNavCss(urlHtml) {
		$("#noti").show();
		$("#notiLine").show();
		
		$("#teamImg").show();
		$("#teamImg").css("opacity", "0.5");
		$("#rankImg").show();
		$("#rankImg").css("opacity", "0.5");
		$("#matchImg").show();
		$("#matchImg").css("opacity", "0.5");
	}

	//url 이동
	function goUrl(obj) {
		var type = $(obj).attr('id');
		console.log(type);
		if (type == "rank" || type == "rankImg") {
			location.href = "/teamRank";
		} else if (type == "match" || type == "matchImg") {
			location.href = "/matchInfo";
		} else if (type == "team" || type == "teamImg") {
			location.href = "/teamList";
		} else {
			location.href = "/notice";
		}
	}
</script>

</html>
