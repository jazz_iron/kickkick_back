<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
	
	<title>KICKKICK</title>
	
</head>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>

	<div class="container imgBackGround">
		<div class="row rankHeaderDiv" id ="rankHeaderDiv">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
<!-- 				<div class="headerImg" id = "headerDiv">
					<img class="logoImg" src="/resources/images/premierLogo.png" ></img>
				</div> -->
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
					<div class= "leagueTitleEng">Premier League</div>
					
					<div class= "leagueTitleKor">프리미어리그</div>
				</div>
			</div>
		</div>
		
		<div class="row" >
			<div class="rankTabDiv" id = "rankTabDiv">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding rankTitle">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>순위</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
						<div>구단</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>경기</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>승</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>무</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>패</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>득</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>실</div>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">
						<div>승점</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" >
			<div class= "rankDiv" id = "rankDiv">
				<div id ="rankListHtml"></div>
			</div>
		</div>
		
		
		<div class="row" >
			<div class="navBarDiv" id = "navBarDiv">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						
						<div class="navbarItem" id="rank" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="rankLine" style="display: none;"></div>순위</div>
						<div class="navbarItem" id="rankImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_rank.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="match" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="matchLine" style="display: none;"></div>경기</div>
						<div class="navbarItem" id="matchImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_match.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="team" onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="teamLine" style="display: none;"></div>구단</div>
						<div class="navbarItem" id="teamImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_team.png" ></img>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-padding table-area">
						<div class="navbarItem" id="noti"  onclick="goUrl(this);" style="display: none;"><div class="navbarLine" id="notiLine" style="display: none;"></div>공지</div>
						<div class="navbarItem" id="notiImg" onclick="goUrl(this);" style="display: none;">
							<img class="navImg" src="/resources/images/icons/icon_notice.png" ></img>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</body>

<script>
	
	var rankListHtml = '';
	var matchDay;
	var matchRound;
	var maxRound;
	
	//Div height default
	var sum ="";
	var windowH =  $(window).outerHeight(true);
	var rankHeaderDiv =  $("#rankHeaderDiv").outerHeight(true);
	var rankTabDivH =  $("#rankTabDiv").outerHeight(true);
	var navBarDivH =  $("#navBarDiv").outerHeight(true);
	
	$( document ).ready( function(){
		//rankList Set
		setRankList("ALL");
		//tab Setting
		toggleNavCss("teamRank");
		//div Settring
		setDiv();
	});
	
	function setDiv(){
		sum = windowH - rankHeaderDiv - rankTabDivH - navBarDivH + "px";
		$("#rankDiv").css("height", sum);
	}
	
	function setRankList(teamId){
		var formData = new FormData();
		formData.append("teamId", "ALL");
		formData.append("areaType", "E"); 
		rankListHtml = '';
		
  		var rank, matchCnt, teamId, teamName ,grade ,win ,draw ,lose ,goalPoint ,losePoint ,gap ,teamImg;
  		
		
		$.ajax({
			type : 'POST',
			url : "/getTeamRank",
			processData : false,
			contentType : false,
			data : formData,
			success : function(data, status) {
				
				//console.table(data.teamRankList);
				$.each(data.teamRankList, function(idx, val) {
					rank = val.rank;
					matchCnt = val.matchCnt;
					teamId = val.teamId;
					teamName = val.teamName;
					grade = val.grade;
					win = val.win;
					draw = val.draw;
					lose = val.lose;
					goalPoint = val.goalPoint;
					losePoint = val.losePoint;
					gap = val.gap;
					teamImg = val.teamImg;
	 				makeRankListHtml(rank,matchCnt,teamId,teamName,grade,win,draw,lose,goalPoint,losePoint,gap,teamImg);
				});
				
			},
			error : function(request, status, error) {
			}
		});
  	}
	
	//경기결과 노출
	function makeRankListHtml(rank,matchCnt,teamId,teamName,grade,win,draw,lose,goalPoint,losePoint,gap,teamImg){
		
		
		if(rank < 5){
			rankListHtml +=	'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">';
			rankListHtml +=	'<div class="rankContent" onclick="detailTeam(this);" id ="'+teamId+'">';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-b">'+rank+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">';
			rankListHtml +=			'<div class="rankTeamName"><img class="rankTeamImg" src="'+teamImg+'" ></img><span class="fc-b">'+teamName+'</span></div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-b">'+matchCnt+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-bb">'+win+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-g">'+draw+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-r">'+lose+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-b">'+goalPoint+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-b">'+losePoint+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-b">'+grade+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=	'</div>';
			rankListHtml +=	'</div>';
		
		}else{
			
			rankListHtml +=	'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">';
			rankListHtml +=	'<div class="rankContent rankContentLow" onclick="detailTeam(this);" id ="'+teamId+'">';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-w" >'+rank+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">';
			rankListHtml +=			'<div class="rankTeamName"><img class="rankTeamImg" src="'+teamImg+'" ></img><span class="fc-w">'+teamName+'</span></div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-w">'+matchCnt+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-bb">'+win+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-g">'+draw+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-r">'+lose+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-w">'+goalPoint+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-w">'+losePoint+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=		'<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no-padding">';
			rankListHtml +=			'<div class="fc-w">'+grade+'</div>';
			rankListHtml +=		'</div>';
			rankListHtml +=	'</div>';
			rankListHtml +=	'</div>';
		}
		
		$('#rankListHtml').html(rankListHtml);
	}
	
	function toggleNavCss(urlHtml){
  		 $("#rankLine").show();
  		 $("#rank").show();
  		 
		 $("#matchImg").show();
		 $("#matchImg").css("opacity","0.5");
		 $("#teamImg").show();
		 $("#teamImg").css("opacity","0.5");
		 $("#notiImg").show();
		 $("#notiImg").css("opacity","0.5");
		 
	}
	
	//url 이동
	function goUrl(obj) {
		var type = $(obj).attr('id');
		if (type == "rank" || type == "rankImg") {
			location.href = "/teamRank";
		} else if (type == "match" || type == "matchImg") {
			location.href = "/matchInfo";
		} else if (type == "team" || type == "teamImg") {
			location.href = "/teamList";
		} else {
			location.href = "/notice";
		}
	}
	
	
	//구단 상세정보 페이지 
	function detailTeam(obj){
		var id = $(obj).attr('id'); //id
		location.href ="/teamInfo?teamId="+id;
	}
  	
</script>

</html>
