<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- 부트스트랩 -->
	<link href="/resources/css/bootstrap.css" rel="stylesheet"/>
	<link href="/resources/css/common.css" rel="stylesheet"/>
	<link href="/resources/css/custom.css" rel="stylesheet"/>
	<link href="/resources/css/micro.css" rel="stylesheet"/>
	<link href="/resources/css/table.css" rel="stylesheet"/>
	<link href="/resources/css/swiper.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"/>
</head>

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>
	<script src="/resources/js/swiper.min.js"></script>
	
	
	
	<div class= "totalAvgContent" id = "totalAvgInfoDiv">
			 
<!-- 	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
	            <div class="avgTitle">경기당 평균 승점</div>
	            <div class="contentDiv">
	                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
	                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">
	                        <div class="teamAvgVal"><span id = "pointGameTT"></span></div>
	                    </div>
	                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
	
	                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">
	                           <div class="progressContent">
	                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">
	                                    <div class="progressItemLeft"></div>
	                                </div>
	                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
	                                    <div class="progressItemRight"></div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">
	                        <div class="leagueAvgVal"><span id = "pointGameTL"></span></div>
	                    </div>
	                </div>
	            </div>
	        </div> -->
	        
	        <div class="" id ="homeTeamInfo"></div>
	        <div class="" id ="awayTeamInfo"></div>
	        <div class="" id ="totalTeamInfo"></div>
	        
	        
	        
	        
	        
     </div>


</body>

<script>



(function getTeamAverage(teamId) {
	var formData = new FormData();
	formData.append("teamId", teamId);

	$.ajax({
		type : 'POST',
		url : "/getTeamAverage",
		processData : false,
		contentType : false,
		data : formData,
		success : function(data, status) {
			//console.log(data.teamAverageList);
			//console.log(data.teamAverageList[0]);

			makeAvgInfoHtml(data.teamAverageList[0] , "#homeTeamInfo");
			makeAvgInfoHtml(data.teamAverageList[1] , "#awayTeamInfo");
			makeAvgInfoHtml(data.teamAverageList[2] , "#totalTeamInfo");

		},
		error : function(request, status, error) {
		}
	});
}('4'));


function makeAvgInfoHtml(obj , type){
	console.log(obj);
	
	$(type).append(
         '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">경기당 평균 승점</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.pointGameH+'</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.pointGameHPer+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.pointGameLPer+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.pointGameH+'</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">승률</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.winsH+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.winsH+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.winsH+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.winsL+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">패배확률</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.defeatsH+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.defeatsH+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.defeatsL+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.defeatsL+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">비길확률</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.drawH+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.drawH+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.drawL+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.drawL+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">경기당 평균 득점</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.scoredGameH+'</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.scoredGameHPer+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.scoredGameLPer+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.scoredGameL+'</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">경기당 평균 실점</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.concededGameH+'</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.concededGameHPer+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.concededGameLPer+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.concededGameL+'</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">경기당 평균 득실점</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.totalGoalH+'</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.totalGoalHPer+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.totalGoalLPer+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.totalGoalL+'</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">득실 > 2.5</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.over25H+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.over25H+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.over25L+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.over25L+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">득실 > 3.5</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.over35H+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.over35H+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.over35L+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.over35L+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
        +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'    <div class="avgTitle">양팀 득점확률</div>'
        +'    <div class="contentDiv">'
        +'       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">'
        +'           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="teamAvgVal">'+obj.bothGoalH+'%</div>'
        +'            </div>'
        +'            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">'
        +'                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding progressDiv">'
        +'                   <div class="progressContent">'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding progressItemLeftDiv">'
        +'                            <div class="progressItemLeft" style ="width:'+obj.bothGoalH+'%"></div>'
        +'                        </div>'
        +'                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">'
        +'                            <div class="progressItemRight" style ="width:'+obj.bothGoalL+'%"></div>'
        +'                        </div>'
        +'                    </div>'
        +'                </div>'
        +'            </div>'
        +'            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">'
        +'                <div class="leagueAvgVal">'+obj.bothGoalL+'%</div>'
        +'            </div>'
        +'        </div>'
        +'    </div>'
        +'</div>'
        
	);
	
	
	
	
	
}



  	
</script>

</html>
